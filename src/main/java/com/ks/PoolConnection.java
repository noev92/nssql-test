package com.ks;

import org.apache.commons.dbcp.BasicDataSource;

import javax.sql.DataSource;
import java.sql.Connection;

/**
 * Creted by Osvaldo Francisco Pérez Ayala
 * KS Soluciones S.A. de C.V.
 * http://www.kssoluciones.com.mx
 * Date: 23/05/2018
 * Time: 05:37 PM
 * Project: nssql_test
 */

public class PoolConnection
{
    private static final PoolConnection INSTANCE = new PoolConnection();
    private DataSource dataSource;

    private PoolConnection()
    {
    }

    public static PoolConnection getInstance()
    {
        return INSTANCE;
    }

    public void initializes()
    {
        String url = "jdbc:sqlmx:";
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName("com.tandem.sqlmx.SQLMXDriver");
        basicDataSource.setUrl(url);
        basicDataSource.setMaxIdle(20);
        basicDataSource.setMaxActive(20);
        dataSource = basicDataSource;
    }

    public synchronized Connection getConnect()
    {
        try
        {
            return dataSource.getConnection();
        }
        catch (Exception ex)
        {
            System.out.println("Problemas al obtener la conexion" + ex.getMessage());
            return null;
        }
    }
}
